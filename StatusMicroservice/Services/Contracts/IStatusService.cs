using System;
using System.Collections.Generic;
using Alten.VS.Shared.Dtos;

namespace Alten.VS.Status.Services.Contracts
{
    public interface IStatusService
    {
        Dictionary<string, VehicleStatusDto> GetAll();
        
        void Update(String vin, DateTime timestamp);
    }
}
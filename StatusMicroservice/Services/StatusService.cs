using System;
using System.Collections.Generic;
using System.Linq;
using Alten.VS.Shared.Dtos;
using Alten.VS.Status.Services.Contracts;
using Microsoft.Extensions.Logging;

namespace Alten.VS.Status.Services
{
    public class StatusService : IStatusService
    {
        private Dictionary<string, VehicleStatusDto> _statuses = new Dictionary<string, VehicleStatusDto>();
        private readonly ILogger<StatusService> _logger;

        public StatusService(ILogger<StatusService> logger)
        {
            _logger = logger;
        }

        public Dictionary<string, VehicleStatusDto> GetAll()
        {
            RecalculateStatuses();
            
            return _statuses;
        }

        public void RecalculateStatuses()
        {
            _statuses.ToList()
                .ForEach(e => Update(e.Key, e.Value.Timestamp));
        }

        public void Update(string vin, DateTime timestamp)
        {
            _logger.LogInformation($"Update information for {vin}");
            
            if (!_statuses.ContainsKey(vin))
            {
                _statuses.Add(vin, new VehicleStatusDto());
            }

            _statuses[vin].Vin = vin;
            _statuses[vin].Status = CalculateStatus(timestamp);
            _statuses[vin].Timestamp = timestamp;
        }

        public Shared.Dtos.Status CalculateStatus(DateTime timestamp)
        {
            _logger.LogInformation($"Calculate status for given timestamp {timestamp}");
            
            if (DateTime.Now.AddSeconds(-60) > timestamp)
            {
                return Shared.Dtos.Status.Offline;
            }

            return Shared.Dtos.Status.Online;
        }
    }
}
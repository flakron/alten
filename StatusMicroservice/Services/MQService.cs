using System;
using System.Text;
using Alten.VS.Shared.Dtos;
using Alten.VS.Status.Services.Contracts;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.MessagePatterns;

namespace Alten.VS.Status.Services
{
    public class MQService
    {
        private static IConnectionFactory _connectionFactory;
        
        public static void Subscribe(ILogger<MQService> logger, IStatusService statusService)
        {   
            _connectionFactory = new ConnectionFactory
            {
                HostName = Environment.GetEnvironmentVariable("QUEUE_HOST"),
                UserName = Environment.GetEnvironmentVariable("QUEUE_USER"), 
                Password = Environment.GetEnvironmentVariable("QUEUE_PASS"),
                AutomaticRecoveryEnabled = true
            };
            
            using(var connection = _connectionFactory.CreateConnection())
            using(var channel = connection.CreateModel())
            {
                var subscription = new Subscription(channel, Environment.GetEnvironmentVariable("QUEUE"), false);

                while (true)
                {
                    var args = subscription.Next();
                        
                    var message = Encoding.Default.GetString(args.Body);

                    var dto = JsonConvert.DeserializeObject<VehicleStatusDto>(message);
                    statusService.Update(dto.Vin, dto.Timestamp);
                    
                    subscription.Ack(args);

                    logger.LogInformation($"Received message: {message}");
                    
                }
            }
        }

    }
}
namespace Alten.VS.Status.Controllers
{
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Mvc;
    using Services.Contracts;
    using Shared.Dtos;

    [Route("api/v1/status")]
    public class StatusController : ControllerBase
    {
        private readonly IStatusService _service;

        public StatusController(IStatusService service)
        {
            _service = service;
        }

        [HttpGet]
        public Dictionary<string, VehicleStatusDto> GetAll()
        {
            return _service.GetAll();
        }
    }
}
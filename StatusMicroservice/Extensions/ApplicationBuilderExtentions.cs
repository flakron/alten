using System.Threading;
using Alten.VS.Status.Services;
using Alten.VS.Status.Services.Contracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Logging;

namespace Alten.VS.Status.Extensions
{
    public static class ApplicationBuilderExtentions
    {
        public static void UseRabbitListener(this IApplicationBuilder app, ILogger<MQService> logger, IStatusService statusService)
        {
            Thread thread = new Thread(() => MQService.Subscribe(logger, statusService));
            thread.Start();
        }
    }
}
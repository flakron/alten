﻿namespace RobotMicroservice
{
    using System.Threading;
    using RobotMicroservice.Robots;

    public static class Program
    {
        public static void Main()
        {
            new Thread(new VehicleRobot().Init).Start();
        }
    }
}
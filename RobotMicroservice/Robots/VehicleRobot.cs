namespace RobotMicroservice.Robots
{
    using System;
    using System.Net.Http;
    using System.Threading;

    public class VehicleRobot
    {
        private readonly string _vin;
        private readonly string _url;
        
        private readonly HttpClient _client;

        private int _fromSeconds = 1;
        private int _toSeconds = 10;

        private int _secondsToSleep = 10;

        public VehicleRobot()
        {
            _client = new HttpClient();
            
            _vin = Environment.GetEnvironmentVariable("VIN_NUMBER");
            _url = Environment.GetEnvironmentVariable("URL");
            
            _fromSeconds = Int32.Parse(Environment.GetEnvironmentVariable("FROM_SECONDS"));
            _toSeconds = Int32.Parse(Environment.GetEnvironmentVariable("TO_SECONDS"));
        }

        public void Init()
        {
            while (true)
            {
                var url = $"{_url}{_vin}";
                
                Console.WriteLine($"Ping: {url}");
                _client.PostAsync(new Uri(url), null);
                
                _secondsToSleep = RandomNumber(_fromSeconds, _toSeconds);
                Console.WriteLine($"Next meeting time in {_secondsToSleep} seconds");
                
                Thread.Sleep(1000 * _secondsToSleep);
            }
        }

        private static int RandomNumber(int min, int max)
        {
            return new Random().Next(min, max);
        }
    }
}
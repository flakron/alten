FROM microsoft/dotnet:2.1-sdk AS build-env

COPY . ./app
WORKDIR /app

RUN dotnet publish -c Release -o out

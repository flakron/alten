namespace Alten.VS.Web.Tests.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Alten.VS.Web.Controllers;
    using Microsoft.Extensions.Logging;
    using Moq;
    using Services.Contracts;
    using Shared.Entities;
    using Xunit;

    public class ClientsControllerTests
    {
        [Fact]
        public async Task Index_ReturnsClients()
        {
            // Prepare
            var logger = new Mock<ILogger<ClientsController>>();
            var service = new Mock<IClientService>();

            service.Setup(e => e.GetAll())
                .ReturnsAsync(new List<Client>());
            
            var controller = new ClientsController(logger.Object, service.Object);

            // Act
            var result = await controller.Index();
            
            // Assert
            Assert.NotNull(result);
        }
    }
}
namespace Alten.VS.Web.Tests.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Alten.VS.Web.Controllers;
    using Microsoft.Extensions.Logging;
    using Moq;
    using Services.Contracts;
    using Shared.Entities;
    using Xunit;

    public class VehiclesControllerTest
    {
        [Fact]
        public async Task Index_ReturnsVehicles()
        {
            // Prepare
            var logger = new Mock<ILogger<VehiclesController>>();
            var service = new Mock<IVehicleService>();

            service.Setup(e => e.GetAll())
                .ReturnsAsync(new List<Vehicle>());
            
            var controller = new VehiclesController(logger.Object, service.Object);

            // Act
            var result = await controller.Index();
            
            // Assert
            Assert.NotNull(result);
        }
    }

}
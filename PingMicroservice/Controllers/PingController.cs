using System;
using Alten.VS.Ping.Services.Contracts;
using Alten.VS.Shared.Dtos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Alten.VS.Ping.Controllers
{
    [Route("api/v1/[controller]")]
    public class PingController : ControllerBase
    {
        private readonly ILogger<PingController> _logger;
        private readonly IMQService _service;

        public PingController(ILogger<PingController> logger, IMQService service)
        {
            _logger = logger;
            _service = service;
        }

        [HttpPost("{vin}")]
        public IActionResult Ping(string vin)
        {
            _logger.LogInformation("Got pinged by {vin}", vin);

            var status = new VehicleStatusDto
            {
                Timestamp = DateTime.Now,
                Vin = vin
            };
            
            _service.Publish(status);
            
            return Ok(vin);
        }
    }
}
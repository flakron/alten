using Alten.VS.Shared.Dtos;

namespace Alten.VS.Ping.Services.Contracts
{
    public interface IMQService
    {
        void Publish(VehicleStatusDto status);
    }
}
using System;
using System.Text;
using Alten.VS.Ping.Services.Contracts;
using Alten.VS.Shared.Dtos;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace Alten.VS.Ping.Services
{
    public class MQService : IMQService
    {
        private readonly IConnectionFactory _connectionFactory;
        private readonly ILogger<MQService> _logger;

        public MQService(ILogger<MQService> logger)
        {
            _logger = logger;
            _connectionFactory = new ConnectionFactory
            {
                HostName = Environment.GetEnvironmentVariable("QUEUE_HOST"),
                UserName = Environment.GetEnvironmentVariable("QUEUE_USER"), 
                Password = Environment.GetEnvironmentVariable("QUEUE_PASS")
            };
        }

        public void Publish(VehicleStatusDto status)
        {
            var queue = Environment.GetEnvironmentVariable("QUEUE");
            var exchange = Environment.GetEnvironmentVariable("EXCHANGE");
            
            using(var connection = _connectionFactory.CreateConnection())
            using(var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: queue,
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);
                
                channel.ExchangeDeclare(exchange, ExchangeType.Topic);
                channel.QueueBind(queue, exchange, "");

                var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(status));

                channel.BasicPublish(exchange: exchange,
                    routingKey: "",
                    basicProperties: null,
                    body: body);
                
                _logger.LogInformation("Published message to MQ");
            }
        }
    }
}
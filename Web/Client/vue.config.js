// vue.config.js
module.exports = {
  outputDir: "../wwwroot",
  devServer: {
    proxy: {
      "/api/*": {
        target: "http://localhost:9003",
        secure: false
      }
    }
  }
};
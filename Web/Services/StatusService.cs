namespace Alten.VS.Web.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Alten.VS.Web.Repositories.Contracts;
    using Contracts;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;
    using Shared.Dtos;
    using Shared.Mappers;

    public class StatusService : IStatusService
    {
        private readonly ILogger<StatusService> _logger;
        private readonly IVehicleRepository _repository;
        private readonly HttpClient _httpClient;

        public StatusService(ILogger<StatusService> logger, IVehicleRepository repository)
        {
            _logger = logger;
            _repository = repository;
            _httpClient = new HttpClient();
        }

        public async Task<IEnumerable<VehicleStatusDto>> GetAll(Guid? clientId, Status? status)
        {
            var result = new List<VehicleStatusDto>();
            
            var vehiclesQuery = _repository
                .GetAll()
                .Include(e => e.Client)
                .AsQueryable();
            
            if (clientId.HasValue && clientId != Guid.Empty)
            {
                vehiclesQuery = vehiclesQuery.Where(e => e.ClientId == clientId.Value);
            }

            var vehicles = vehiclesQuery.ToList();
            var statuses = await GetVehiclesStatuses();

            foreach (var vehicle in vehicles)
            {
                var vehicleStatus = Status.Offline;
                var vehicleTimestamp = new DateTime();
                
                if (statuses.ContainsKey(vehicle.Vin))
                {
                    vehicleStatus = statuses[vehicle.Vin].Status;
                    vehicleTimestamp = statuses[vehicle.Vin].Timestamp;
                }
                
                result.Add(new VehicleStatusDto
                {
                    Vehicle = VehicleMapper.Map(vehicle),
                    Vin = vehicle.Vin,
                    Status = vehicleStatus,
                    Timestamp = vehicleTimestamp
                });
            }

            if (status != null)
            {
                result = result.Where(e => e.Status == status).ToList();
            }

            return result;
        }

        private async Task<Dictionary<string, VehicleStatusDto>> GetVehiclesStatuses()
        {
            // Contact the status microservice
            var response = await _httpClient.GetAsync(Environment.GetEnvironmentVariable("STATUS_API"));
            if (!response.IsSuccessStatusCode)
            {
                _logger.LogError("An error occurred when trying to fetch vehicle statuses");
            }
            
            var resultText = await response.Content.ReadAsStringAsync();
            
            return JsonConvert.DeserializeObject<Dictionary<string, VehicleStatusDto>>(resultText);
        }
    }
}
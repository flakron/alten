namespace Alten.VS.Web.Services
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Alten.VS.Web.Repositories.Contracts;
    using Contracts;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Shared.Entities;

    public class VehicleService : IVehicleService
    {
        private readonly IVehicleRepository _repository;
        private readonly ILogger<VehicleService> _logger;

        public VehicleService(IVehicleRepository repository, ILogger<VehicleService> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task<IEnumerable<Vehicle>> GetAll()
        {
            _logger.LogInformation("GetAll vehicles");
            return await _repository.GetAll().ToListAsync();
        }
    }
}
namespace Alten.VS.Web.Services
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Alten.VS.Web.Repositories.Contracts;
    using Contracts;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Shared.Entities;

    public class ClientService : IClientService
    {
        private readonly IClientRepository _repository;
        private readonly ILogger<ClientService> _logger;

        public ClientService(IClientRepository repository, ILogger<ClientService> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task<IEnumerable<Client>> GetAll()
        {
            _logger.LogInformation("Get all clients");
            return await _repository.GetAll().ToListAsync();
        }
    }
}
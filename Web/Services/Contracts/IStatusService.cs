using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Alten.VS.Shared.Dtos;

namespace Alten.VS.Web.Services.Contracts
{
    public interface IStatusService
    {
        Task<IEnumerable<VehicleStatusDto>> GetAll(Guid? clientId, Status? status);
    }
}
using System.Collections.Generic;
using System.Threading.Tasks;
using Alten.VS.Shared.Entities;

namespace Alten.VS.Web.Services.Contracts
{
    public interface IVehicleService
    {
        Task<IEnumerable<Vehicle>> GetAll();
    }
}
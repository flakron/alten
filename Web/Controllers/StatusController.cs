namespace Alten.VS.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Services.Contracts;
    using Shared.Dtos;

    [Route("api/v1/status")]
    public class StatusController : Controller
    {
        private readonly ILogger<StatusController> _logger;
        private readonly IStatusService _service;

        public StatusController(ILogger<StatusController> logger, IStatusService service)
        {
            _logger = logger;
            _service = service;
        }

        [HttpGet("{clientId?}/{status?}")]
        public async Task<IEnumerable<VehicleStatusDto>> Index([FromRoute] Guid? clientId, [FromRoute] Status? status)
        {
            _logger.LogInformation("Get vehicle status");
            return await _service.GetAll(clientId, status);
        }
    }
}
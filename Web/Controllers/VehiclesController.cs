namespace Alten.VS.Web.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Services.Contracts;
    using Shared.Dtos;
    using Shared.Mappers;

    [Route("api/v1/vehicles")]
    public class VehiclesController
    {
        private readonly ILogger<VehiclesController> _logger;
        private readonly IVehicleService _service;

        public VehiclesController(ILogger<VehiclesController> logger, IVehicleService service)
        {
            _logger = logger;
            _service = service;
        }

        [HttpGet]
        public async Task<IEnumerable<VehicleDto>> Index()
        {
            _logger.LogInformation("Get all vehicles");
            return VehicleMapper.Map(await _service.GetAll());
        }
    }
}
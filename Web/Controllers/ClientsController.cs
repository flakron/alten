using Microsoft.Extensions.Logging;

namespace Alten.VS.Web.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Services.Contracts;
    using Shared.Dtos;
    using Shared.Mappers;

    [Route("api/v1/clients")]
    public class ClientsController : Controller
    {
        private readonly IClientService _service;
        private readonly ILogger<ClientsController> _logger;

        public ClientsController(ILogger<ClientsController> logger, IClientService service)
        {
            _logger = logger;
            _service = service;
        }

        [HttpGet]
        public async Task<IEnumerable<ClientDto>> Index()
        {
            _logger.LogInformation("Get all clients");
            return ClientMapper.Map(await _service.GetAll());
        }
    }
}
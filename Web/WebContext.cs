using System;
using Alten.VS.Shared.Entities;
using Microsoft.EntityFrameworkCore;

namespace Alten.VS.Web
{
    public class WebContext : DbContext
    {    
        public DbSet<Client> Clients { get; set; }
        
        public DbSet<Vehicle> Vehicles { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=web.db");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            var client1 = new Client
            {
                Id = Guid.NewGuid(),
                Name = "Kalles Grustransporter AB",
                Address = "111 11 Södertälje"
            };

            var client2 = new Client
            {
                Id = Guid.NewGuid(),
                Name = "Johans Bulk AB",
                Address = "222 22 Stockholm"
            };

            var client3 = new Client
            {
                Id = Guid.NewGuid(),
                Name = "Haralds Värdetransporter AB",
                Address = "333 33 Uppsala"
            };
            
            builder.Entity<Client>().HasData(client1, client2, client3);
            builder.Entity<Vehicle>().HasData(
                // Client 1
                new Vehicle
                {
                    Id = Guid.NewGuid(),
                    Vin = "YS2R4X20005399401",
                    RegistrationNumber = "ABC123",
                    ClientId = client1.Id
                },
                new Vehicle
                {
                    Id = Guid.NewGuid(),
                    Vin = "VLUR4X20009093588",
                    RegistrationNumber = "DEF456",
                    ClientId = client1.Id
                },
                new Vehicle
                {
                    Id = Guid.NewGuid(),
                    Vin = "VLUR4X20009048066",
                    RegistrationNumber = "GHI789",
                    ClientId = client1.Id
                },
                // Client 2
                new Vehicle
                {
                    Id = Guid.NewGuid(),
                    Vin = "YS2R4X20005388011",
                    RegistrationNumber = "JKL012",
                    ClientId = client2.Id
                },
                new Vehicle
                {
                    Id = Guid.NewGuid(),
                    Vin = "YS2R4X20005387949",
                    RegistrationNumber = "MNO345",
                    ClientId = client2.Id
                },
                // Client 3
                new Vehicle
                {
                    Id = Guid.NewGuid(),
                    Vin = "VLUR4X20009048067",
                    RegistrationNumber = "PQR678",
                    ClientId = client3.Id
                },
                new Vehicle
                {
                    Id = Guid.NewGuid(),
                    Vin = "YS2R4X20005387055",
                    RegistrationNumber = "STU901",
                    ClientId = client3.Id
                }
            );
        }
    }
}
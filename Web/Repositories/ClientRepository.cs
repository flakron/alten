using Alten.VS.Shared.Entities;
using Alten.VS.Web.Repositories.Contracts;

namespace Alten.VS.Web.Repositories
{
    public class ClientRepository : GenericRepository<Client>, IClientRepository
    {
        public ClientRepository(WebContext context) : base(context)
        {
        }
    }
}
using Alten.VS.Shared.Entities;
using Alten.VS.Web.Repositories.Contracts;

namespace Alten.VS.Web.Repositories
{
    public class VehicleRepository : GenericRepository<Vehicle>, IVehicleRepository
    {
        public VehicleRepository(WebContext context) : base(context)
        {
        }
    }
}
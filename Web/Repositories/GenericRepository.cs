using System;
using System.Linq;
using System.Threading.Tasks;
using Alten.VS.Shared.Entities.Contracts;
using Alten.VS.Web.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Alten.VS.Web.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity>
        where TEntity : class, IEntity
    {
        private readonly WebContext _context;

        public GenericRepository(WebContext context)
        {
            _context = context;
        }

        public IQueryable<TEntity> GetAll()
        {
            return _context.Set<TEntity>()
                .AsNoTracking();
        }

        public async Task<TEntity> GetById(Guid id)
        {
            return await _context.Set<TEntity>()
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task Create(TEntity entity)
        {
            await _context.Set<TEntity>()
                .AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Update(TEntity entity)
        {
            _context.Set<TEntity>()
                .Update(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Guid id)
        {
            var entity = await GetById(id);
            _context.Set<TEntity>()
                .Remove(entity);
            
            await _context.SaveChangesAsync();
        }
    }
}
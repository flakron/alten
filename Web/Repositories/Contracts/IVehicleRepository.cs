using Alten.VS.Shared.Entities;

namespace Alten.VS.Web.Repositories.Contracts
{
    public interface IVehicleRepository : IGenericRepository<Vehicle>
    {
        
    }
}
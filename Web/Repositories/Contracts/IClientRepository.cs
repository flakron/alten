using Alten.VS.Shared.Entities;

namespace Alten.VS.Web.Repositories.Contracts
{
    public interface IClientRepository : IGenericRepository<Client>
    {
        
    }
}
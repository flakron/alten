using System;
using System.Linq;
using System.Threading.Tasks;
using Alten.VS.Shared.Entities.Contracts;

namespace Alten.VS.Web.Repositories.Contracts
{
    public interface IGenericRepository<TEntity>
        where TEntity : class, IEntity
    {
        IQueryable<TEntity> GetAll();

        Task<TEntity> GetById(Guid id);

        Task Create(TEntity entity);

        Task Update(TEntity entity);

        Task Delete(Guid id);
    }
}
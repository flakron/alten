﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Alten.VS.Web.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Clients",
                columns: new[] { "Id", "Address", "Name" },
                values: new object[] { new Guid("850e56e2-56bd-46c3-87d1-396d846c12a4"), "111 11 Södertälje", "Kalles Grustransporter AB" });

            migrationBuilder.InsertData(
                table: "Clients",
                columns: new[] { "Id", "Address", "Name" },
                values: new object[] { new Guid("a179dc28-be97-4393-922c-96e30290569f"), "222 22 Stockholm", "Johans Bulk AB" });

            migrationBuilder.InsertData(
                table: "Clients",
                columns: new[] { "Id", "Address", "Name" },
                values: new object[] { new Guid("4decd017-4a37-4591-8f23-569ffc7c1738"), "333 33 Uppsala", "Haralds Värdetransporter AB" });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "Id", "ClientId", "RegistrationNumber", "Vin" },
                values: new object[] { new Guid("f9f49150-8786-476c-8423-5cfae490fe29"), new Guid("850e56e2-56bd-46c3-87d1-396d846c12a4"), "ABC123", "YS2R4X20005399401" });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "Id", "ClientId", "RegistrationNumber", "Vin" },
                values: new object[] { new Guid("e0dd51e3-9d42-4107-8d0a-4e3daa77baec"), new Guid("850e56e2-56bd-46c3-87d1-396d846c12a4"), "DEF456", "VLUR4X20009093588" });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "Id", "ClientId", "RegistrationNumber", "Vin" },
                values: new object[] { new Guid("f2d273e0-195d-4ad4-8140-789bf30d6239"), new Guid("850e56e2-56bd-46c3-87d1-396d846c12a4"), "GHI789", "VLUR4X20009048066" });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "Id", "ClientId", "RegistrationNumber", "Vin" },
                values: new object[] { new Guid("0ac93ae4-55e0-43e2-9788-9003619bcc93"), new Guid("a179dc28-be97-4393-922c-96e30290569f"), "JKL012", "YS2R4X20005388011" });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "Id", "ClientId", "RegistrationNumber", "Vin" },
                values: new object[] { new Guid("1fe25c7d-b68c-488d-9fb8-96a1c4dc748b"), new Guid("a179dc28-be97-4393-922c-96e30290569f"), "MNO345", "YS2R4X20005387949" });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "Id", "ClientId", "RegistrationNumber", "Vin" },
                values: new object[] { new Guid("0c325be2-764d-4519-b596-8e0b14b87a5c"), new Guid("4decd017-4a37-4591-8f23-569ffc7c1738"), "PQR678", "VLUR4X20009048067" });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "Id", "ClientId", "RegistrationNumber", "Vin" },
                values: new object[] { new Guid("befbd829-ca59-440d-af5b-c610a8be862a"), new Guid("4decd017-4a37-4591-8f23-569ffc7c1738"), "STU901", "YS2R4X20005387055" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Vehicles",
                keyColumn: "Id",
                keyValue: new Guid("0ac93ae4-55e0-43e2-9788-9003619bcc93"));

            migrationBuilder.DeleteData(
                table: "Vehicles",
                keyColumn: "Id",
                keyValue: new Guid("0c325be2-764d-4519-b596-8e0b14b87a5c"));

            migrationBuilder.DeleteData(
                table: "Vehicles",
                keyColumn: "Id",
                keyValue: new Guid("1fe25c7d-b68c-488d-9fb8-96a1c4dc748b"));

            migrationBuilder.DeleteData(
                table: "Vehicles",
                keyColumn: "Id",
                keyValue: new Guid("befbd829-ca59-440d-af5b-c610a8be862a"));

            migrationBuilder.DeleteData(
                table: "Vehicles",
                keyColumn: "Id",
                keyValue: new Guid("e0dd51e3-9d42-4107-8d0a-4e3daa77baec"));

            migrationBuilder.DeleteData(
                table: "Vehicles",
                keyColumn: "Id",
                keyValue: new Guid("f2d273e0-195d-4ad4-8140-789bf30d6239"));

            migrationBuilder.DeleteData(
                table: "Vehicles",
                keyColumn: "Id",
                keyValue: new Guid("f9f49150-8786-476c-8423-5cfae490fe29"));

            migrationBuilder.DeleteData(
                table: "Clients",
                keyColumn: "Id",
                keyValue: new Guid("4decd017-4a37-4591-8f23-569ffc7c1738"));

            migrationBuilder.DeleteData(
                table: "Clients",
                keyColumn: "Id",
                keyValue: new Guid("850e56e2-56bd-46c3-87d1-396d846c12a4"));

            migrationBuilder.DeleteData(
                table: "Clients",
                keyColumn: "Id",
                keyValue: new Guid("a179dc28-be97-4393-922c-96e30290569f"));
        }
    }
}

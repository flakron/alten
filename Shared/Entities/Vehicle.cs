using System;
using Alten.VS.Shared.Entities.Contracts;

namespace Alten.VS.Shared.Entities
{
    public class Vehicle : IEntity
    {
        public Guid Id { get; set; }
        
        public string Vin { get; set; }
        
        public string RegistrationNumber { get; set; }
        
        public Guid ClientId { get; set; }
        public Client Client { get; set; }
    }
}
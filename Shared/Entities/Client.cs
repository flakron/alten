using System;
using Alten.VS.Shared.Entities.Contracts;

namespace Alten.VS.Shared.Entities
{
    public class Client : IEntity
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
        
        public string Address { get; set; }
    }
}
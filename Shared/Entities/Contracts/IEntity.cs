using System;

namespace Alten.VS.Shared.Entities.Contracts
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}
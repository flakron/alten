using System;

namespace Alten.VS.Shared.Dtos
{
    public class ClientDto
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
        
        public string Address { get; set; }
    }
}
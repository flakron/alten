using System;

namespace Alten.VS.Shared.Dtos
{
    public class VehicleStatusDto
    {
        public string Vin { get; set; }
        
        public DateTime Timestamp { get; set; }
        
        public Status Status { get; set; }

        public string StatusText => Status.ToString();
        
        public VehicleDto Vehicle { get; set; }
    }

    public enum Status
    {
        Online,
        Offline
    }
}
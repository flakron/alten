using System;

namespace Alten.VS.Shared.Dtos
{
    public class VehicleDto
    {
        public Guid Id { get; set; }
        
        public string Vin { get; set; }
        
        public string RegistrationNumber { get; set; }
        
        public Guid ClientId { get; set; }
        public ClientDto Client { get; set; }
    }
}
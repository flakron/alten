using System.Collections.Generic;
using System.Linq;
using Alten.VS.Shared.Dtos;
using Alten.VS.Shared.Entities;

namespace Alten.VS.Shared.Mappers
{
    public class VehicleMapper
    {
        public static IEnumerable<VehicleDto> Map(IEnumerable<Vehicle> entities)
        {
            return entities.Select(Map);
        }
        
        public static VehicleDto Map(Vehicle entity)
        {
            if (entity == null)
            {
                return null;
            }
            
            var dto = new VehicleDto
            {
                Id = entity.Id,
                Vin = entity.Vin,
                RegistrationNumber = entity.RegistrationNumber,
                ClientId = entity.ClientId
            };

            if (entity.Client != null)
            {
                dto.Client = ClientMapper.Map(entity.Client);
            }

            return dto;
        }
    }
}
using System.Collections.Generic;
using System.Linq;
using Alten.VS.Shared.Dtos;
using Alten.VS.Shared.Entities;

namespace Alten.VS.Shared.Mappers
{
    public class ClientMapper
    {
        public static IEnumerable<ClientDto> Map(IEnumerable<Client> entities)
        {
            return entities.Select(Map);
        }

        public static ClientDto Map(Client entity)
        {
            if (entity == null)
            {
                return null;
            }
            
            return new ClientDto
            {
                Id = entity.Id,
                Name = entity.Name,
                Address = entity.Address
            };
        }
    }
}
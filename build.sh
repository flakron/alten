#!/usr/bin/env bash

dotnet publish -c Release -o out

docker build -t flakron/alten-ping-microservice PingMicroservice/
docker build -t flakron/alten-robot-microservice RobotMicroservice/
docker build -t flakron/alten-status-microservice StatusMicroservice/
docker build -t flakron/alten-web-microservice Web/


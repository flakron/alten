# Vehicle Status Dashboard

# Issues
- Issue #1 - Time constraints to try-out more solutions and to improve the quality of the 
code. There are some quality issues with the code (which you will see) due to this.
- Issue #2 - Issues signing up with GCP, Azure and Amazon.
- Issue #3 - Lack of experience in using any of the Microservices Chassis Framework.
While I did find those interesting, due to #1 had to leave it for the end (couldn't
complete it).

# Results

Due to limited available time (which I will refer to as ISSUE#1 from now on), some of the 
requirements I could not finish (or some partially), either way,
below you can find the results.

## Requirements results
### 1. Web GUI with a SPA framework.
I completed this using VueJS SPA Framework, due to it's simplicity and my experience
with it, it was a perfect fit for this sample project.
    
The dashboard is running at [http://138.197.190.253:9003/#/](http://138.197.190.253:9003/).
You can filter by client or status as required. The dashboard is auto-updated by polling the 
API, WebSockets would be a better fit for such use cases, but due to limited time, had to 
go with polling.

### 2. Random status sending simulation
I created some simple robots that simulate random status sending. There is no specific random
algorithm behind, depending on the configuration it will choose a random value between two 
int values when to ping the api next time.

### 3. If database design will consume a lot of time, use data in-memory representation.
I used sqlite, it proved to be perfect for this use case.

### 4. Unit testing
This was quite partially done unfortunately due to ISSUE#1

### 5. .NET Core, Java or any native language
I used .NET Core since I applied for .NET position. But personally if given the freedom
would've gone with Java using Spring-Boot since it and its tools are more mature (Spring-Cloud
comes to mind).

### 6. Complete analysis for the problem.
Not completed

#### 6.1 Full architectural sketch to the problem.
![Diragram](diagram.png "Diagram")


#### 6.2 Analysis behind the solution design, technologies.

Projects (microservices) are separated by their role.

- PingMicroservice:
    It's role is to serve an API endpoint for the vehicles to ping, after the ping is received
    the microservice then publishes a message to RabbitMQ.
- StatusMicroservice:
    This microservice is subscribed to the RabbitMQ queue so when a message is received,
    it updates it's list of vehicles statuses. It also has an endpoint for the WebMicroservice
    to consume for the calculated statuses. On every request the 
- WebMicroservice:
    This microservice has a couple of endpoints (this could have been split into more 
    microservices). Upon startup the application runs the migrations and seeds the data 
    (in a real production project migrations/seeding would be handled separately). 
    It serves the frontend dashboard which can call 3 API endpoints.
    - Vehicles API endpoint: serves a list of vehicles
    - Clients API endpoint: serves a list of clients
    - Status API endpoint: this endpoint takes the list of vehicles and contacts the 
    StatusMicroservices to get the statuses for the given vehicles.
 - Shared: Here are stored the shared entities, dtos and their mappers.

##### Technologies, tools and frameworks

Docker orchestration tool:

- [Rancher](https://rancher.com/) (Cattle)

Frontend:

- NodeJS
- VueJS
- Bootstrap
- Tools: Vue CLI

Backend:

- .NET Core 2.1
- Frameworks: ASP.NET MVC, EntityFramework

Data store:

- SQLite

Message broker:

- RabbitMQ

#### 6.3 How the solution will make use of the cloud.
By using docker the solution can be used in all cloud offerings (AWS, Amazon, GCP). The running
applicaiton at [http://138.197.190.253:9003/#/](http://138.197.190.253:9003/) has been started
in [Rancher](https://rancher.com/) (it's an enterprise management for Kubernetes, Docker Swarm and
Cattle).


The initial plan was to use [Consul](https://www.consul.io/) for service discovery and configuration
management, but was unable to complete (ISSUE#1). Which would allow to start more containers and load balance
the requests between them.

#### 6.4 Deployment steps.

Since I already have a paid plan with BitBucket, I chose to use their Pipelines solution.
This allowed me to move much more faster regarding. 


There are two main workflows the master branch (production) and feature branches (development).


The following workflows are fully automatic without any human intervention.

Production workflow:

- The whole solution is compiled (frontend and backend services).
- Docker images are built for every project (versioning the images was planned, but couldn't complete
 ISSUE#1)
- Docker images are pushed to DockerHub
- DockerHub makes a request (via WebHooks) to Rancher to initiate container upgrade to the newer version

Development workflow:

- Code is cloned and the solution is built
    - In this step there is also a static code analysis done
- Frontend project is built
- Backend projects are built
- And finally unit tests are run and with it two reports are generate, the code coverage
report and unit test results

## Optional requirements results
### 1. Write an integration test.
Not completed due to ISSUE#1.

### 2. Write an automation test.
Not completed due to ISSUE#1.

### 3. Use CI to verify code and tests.
I used BitBucket Pipelines for CI tasks. Pipelines will run tasks depending the branch.


Master branch will build the whole solution and create docker images for each project,
and then push them to DockerHub.


### 4. Dockerize the whole solution.
Completed. If you want to run the whole solution locally, you need to have Docker and 
Docker compose installed. Go to the project folder and run
```docker-compose up```. After the containers are started, open your browser and go to
[http://localhost:9003/](http://localhost:9003/).


### 5. Microservice architecture for the driver, vehicle and FaaS APIs.
- Use any Microservices Chassis Framework.

Not completed.

I already spent the free tiers on GCP, and AWS (trying out their services). While Azure
 wouldn't validate my phone and credit card (unrelated, but had to spend a bit time trying).

### 6. Explain if it is possible to be in a Serverless architecture and how.

Since I used .NET Core since Amazon (also Azure) has added support for .NET Core in their serverless
architecture (references)[https://aws.amazon.com/blogs/developer/serverless-asp-net-core-2-0-applications/]
[https://docs.microsoft.com/en-us/dotnet/standard/serverless-architecture/azure-functions]
, it looks quite straight-forward actually. Unfortunately due to ISSUE#2 I wasn't
able to try it out.

Here it is seen where the real strength of Java is, almost all serverless-architectures seem
to support it. While .NET Core is catching up, it still has a long way to go (with the two main
cloud services having support, it doesn't look like a problem).
using System;
using System.Collections.Generic;
using System.Linq;
using Alten.VS.Shared.Entities;
using Alten.VS.Shared.Mappers;
using Xunit;

namespace Alten.VS.Shared.Tests.Mappers
{
    public class VehicleMapperTests
    {
        [Fact]
        public void Map_Entity_ToDto()
        {
            // Prepare
            var entity = new Vehicle
            {
                Id = Guid.NewGuid(),
                Vin = "some vin",
                RegistrationNumber = "some registration number",
                ClientId = Guid.NewGuid()
            };
            
            // Act
            var dto = VehicleMapper.Map(entity);
            
            // Assert
            Assert.Equal(entity.Id, dto.Id);
            Assert.Equal(entity.Vin, dto.Vin);
            Assert.Equal(entity.RegistrationNumber, dto.RegistrationNumber);
            Assert.Equal(entity.ClientId, dto.ClientId);
        }

        [Fact]
        public void Map_NullEntity_ReturnsNull()
        {
            // Prepare
            Vehicle entity = null;
            
            // Act
            var dto = VehicleMapper.Map(entity);
            
            // Assert
            Assert.Null(dto);
        }

        [Fact]
        public void Map_CollectionEntities_ToDtos()
        {
            // Prepare
            var entities = new List<Vehicle>
            {
                new Vehicle(), new Vehicle()
            };
            
            // Act
            var dtos = VehicleMapper.Map(entities);
            
            // Assert
            Assert.Equal(entities.Count, dtos.Count());
        }
    }
}
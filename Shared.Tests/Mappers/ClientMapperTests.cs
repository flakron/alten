using System;
using System.Collections.Generic;
using System.Linq;
using Alten.VS.Shared.Entities;
using Alten.VS.Shared.Mappers;
using Xunit;

namespace Alten.VS.Shared.Tests.Mappers
{
    public class ClientMapperTests
    {
        [Fact]
        public void Map_ToDto()
        {
            // Prepare
            var entity = new Client
            {
                Id = Guid.NewGuid(),
                Name = "some name",
                Address = "some address"
            };
            
            // Act
            var dto = ClientMapper.Map(entity);
            
            // Assert
            Assert.Equal(entity.Id, dto.Id);
            Assert.Equal(entity.Name, dto.Name);
            Assert.Equal(entity.Address, dto.Address);
        }

        [Fact]
        public void Map_NullEntity_ReturnsNull()
        {
            // Prepare
            Client entity = null;
            
            // Act
            var dto = ClientMapper.Map(entity);
            
            // Assert
            Assert.Null(dto);
        }

        [Fact]
        public void Map_CollectionEntities_ToDtos()
        {
            // Prepare
            var entities = new List<Client>
            {
                new Client(),
                new Client()
            };
            
            // Act
            var dtos = ClientMapper.Map(entities);
            
            // Assert
            Assert.Equal(entities.Count, dtos.Count());
        }
    }
}